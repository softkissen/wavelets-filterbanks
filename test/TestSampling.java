import dataStructure.ImageData;
import filter.FilterManager;
import filter.data.FilterMatrix;
import org.junit.jupiter.api.*;


import java.io.IOException;


class TestSampling {

    ImageData testData = new ImageData(4,4);
    private static String imagePath = "hase.png";
    private static String imagePathGray = "grauerHase.png";

    private ImageData createTestData(){
        for (int i = 0; i < testData.getHeight(); i++ ) {
            for (int j = 0; j < testData.getWidth(); j++) {
                testData.setRedDataPixel(i, j, (int) (Math.random() * 10));
                testData.setBlueDataPixel(i, j, (int) (Math.random() * 10));
                testData.setGreenDataPixel(i, j, (int) (Math.random() * 10));
            }
        }
        return testData;
    }

    @Test
    void testImageToChannels() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePath);
        ImageData redImage = new ImageData(loadedImageData.getHeight(), loadedImageData.getWidth());
        ImageData greenImage = new ImageData(loadedImageData.getHeight(), loadedImageData.getWidth());
        ImageData blueImage = new ImageData(loadedImageData.getHeight(), loadedImageData.getWidth());

        redImage.setRedData(loadedImageData.getRedData());
        greenImage.setGreenData(loadedImageData.getGreenData());
        blueImage.setBlueData(loadedImageData.getBlueData());

        filterManager.saveImageData(redImage,"redImage.png");
        filterManager.saveImageData(greenImage,"greenImage.png");
        filterManager.saveImageData(blueImage,"blueImage.png");

    }

    @Test
    void testImageLoadAndSave() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePath);
        ImageData savedImageData = filterManager.saveImageData(loadedImageData, "savedImageData.png");
        ImageData savedLoadedAgain = filterManager.loadImage("savedImageData.png");
        filterManager.compareImages(loadedImageData,savedImageData,"compareLoadedAndSaved.png");
        filterManager.compareImages(loadedImageData,savedLoadedAgain,"compareLoadedAndSavedLoadedAgain.png");
    }
    @Test
    void testDownAndUpSample() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePath);
        ImageData downsampledImageDataHorizontal = filterManager.downSampleImageDataHorizontal(loadedImageData);
        ImageData downsampledImageDataVertical = filterManager.downSampleImageDataVertical(downsampledImageDataHorizontal);
        ImageData downsampledImageData = filterManager.saveImageData(downsampledImageDataVertical, "downsampledImageData.png");


        ImageData upsampledImageDataVertical = filterManager.upSampleImageDataVertical(downsampledImageData);
        filterManager.saveImageData(upsampledImageDataVertical, "upImageDataVertical.png");
        ImageData upsampledImageDataHorizontal = filterManager.upSampleImageDataHorizontal(upsampledImageDataVertical);
        ImageData upImageData = filterManager.saveImageData(upsampledImageDataHorizontal, "upImageData.png");
    }

    @Test
    void testDownAndUpSampleGrayImage() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePathGray);
        ImageData downsampledImageDataHorizontal = filterManager.downSampleImageDataHorizontal(loadedImageData);
        ImageData downsampledImageDataVertical = filterManager.downSampleImageDataVertical(downsampledImageDataHorizontal);
        ImageData downsampledImageData = filterManager.saveImageData(downsampledImageDataVertical, "downsampledImageDataGray.png");
        ImageData upsampledImageDataVertical = filterManager.upSampleImageDataVertical(downsampledImageData);
        ImageData upsampledImageDataHorizontal = filterManager.upSampleImageDataHorizontal(upsampledImageDataVertical);
        ImageData upImageData = filterManager.saveImageData(upsampledImageDataHorizontal, "upImageDataGray.png");
    }

    @Test
    void testFiltering() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePath);
        ImageData horizontalFiltered = filterManager.applyFilter1DHorizontalHaar(loadedImageData, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        filterManager.saveImageData(horizontalFiltered,"horizontalFiltered.png");
        ImageData verticalFiltered = filterManager.applyFilter1DVerticalHaar(loadedImageData, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        filterManager.saveImageData(verticalFiltered,"verticalFiltered.png");
    }

    @Test
    void testDownAndUpSamplingWithFilter() throws IOException {
        FilterManager filterManager = new FilterManager();
        ImageData loadedImageData = filterManager.loadImage(imagePath);

        ImageData filteredHorizontal = filterManager.applyFilter1DHorizontalHaar(loadedImageData, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        filterManager.saveImageData(filteredHorizontal, "filteredImageDataHorizontal.png");

        ImageData downsampledImageDataHorizontal = filterManager.downSampleImageDataHorizontal(filteredHorizontal);
        filterManager.saveImageData(downsampledImageDataHorizontal, "filteredAndDownsampledImageDataHorizontal.png");

        ImageData filteredVertical = filterManager.applyFilter1DVerticalHaar(downsampledImageDataHorizontal, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        filterManager.saveImageData(filteredVertical, "filteredImageDataVertical.png");
        ImageData downsampledImageDataVertical = filterManager.downSampleImageDataVertical(filteredVertical);
        ImageData analysisImageData = filterManager.saveImageData(downsampledImageDataVertical, "analysis.png");


        ImageData upsampledImageDataVertical = filterManager.upSampleImageDataVertical(analysisImageData);
        filterManager.saveImageData(upsampledImageDataVertical, "upImageDataVertical.png");
        ImageData upsampledImageDataVerticalAndFilteredVertical = filterManager.applyFilter1DVerticalHaar(upsampledImageDataVertical, FilterMatrix.HAAR_LOW_PASS_1D);
        filterManager.saveImageData(upsampledImageDataVerticalAndFilteredVertical, "upImageDataVerticalAndFilteredVertical.png");

        ImageData upsampledImageDataHorizontal = filterManager.upSampleImageDataHorizontal(upsampledImageDataVerticalAndFilteredVertical);
        filterManager.saveImageData(upsampledImageDataHorizontal, "upImageDataHorizontal.png");
        ImageData upsampledImageDataHorizontalAndFilteredHorizontal = filterManager.applyFilter1DHorizontalHaar(upsampledImageDataHorizontal, FilterMatrix.HAAR_LOW_PASS_1D);
        filterManager.saveImageData(upsampledImageDataHorizontalAndFilteredHorizontal, "upImageDataHorizontalAndFilteredHorizontal.png");



    }

    @Test
     void testVerticalDownsampling(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataVertical(testData);
        printData();
    }

    @Test
    void testVerticalDownAndUpsampling(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataVertical(testData);
        printData();
        testData= filterManager.upSampleImageDataVertical(testData);
        printData();
    }
    @Test
     void testHorizontalDownsampling(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataHorizontal(testData);
        printData();
    }
    @Test
     void testHorzontallDownAndUpsampling(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataHorizontal(testData);
        printData();
        testData= filterManager.upSampleImageDataHorizontal(testData);
        printData();
    }

    @Test
     void testApplyFilterHorizontal(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataHorizontal(testData);
        printData();
        testData= filterManager.applyFilter1DHorizontalFBI(testData, FilterMatrix.FBI_HIGH_PASS_1D);
        printData();
    }
    @Test
     void testApplyFilterVertical(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        testData= filterManager.downSampleImageDataVertical(testData);
        printData();
        testData= filterManager.applyFilter1DVerticalHaar(testData, FilterMatrix.HAAR_LOW_PASS_1D);
        printData();
    }

    @Test
     void testFirstBranchOfFilterbank(){
        FilterManager filterManager = new FilterManager();
        testData=createTestData();
        printData();
        testData = filterManager.applyFilter1DHorizontalFBI(testData, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        printData();
        testData= filterManager.downSampleImageDataHorizontal(testData);
        printData();
        testData = filterManager.applyFilter1DVerticalFBI(testData, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        printData();
        testData = filterManager.downSampleImageDataVertical(testData);
        printData();
        //reverse
        testData = filterManager.upSampleImageDataVertical(testData);
        printData();
        testData = filterManager.applyFilter1DVerticalFBI(testData, FilterMatrix.FBI_LOW_PASS_1D);
        printData();
        testData = filterManager.upSampleImageDataHorizontal(testData);
        printData();
        testData = filterManager.applyFilter1DHorizontalFBI(testData, FilterMatrix.FBI_LOW_PASS_1D);
        printData();
    }

    @Test
     void testApplyFilter2D(){
        FilterManager filterManager = new FilterManager();
        testData = createTestData();
        printData();
        System.out.println(testData.getHeight()+" "+testData.getWidth());
        System.out.println();
        testData = filterManager.applyFilter(testData, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV);
        printData();
        System.out.println(testData.getHeight()+" "+testData.getWidth());
        System.out.println();
    }
    @Test
    public void testQuincunx() throws IOException {
        FilterManager filterManager = new FilterManager();
        testData = filterManager.loadImage("baboon.png");
        testData = filterManager.applyFilter(testData, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV);
        filterManager.saveImageData(testData,"baboonLowPassFiltered.png");
        testData = filterManager.applyFilter(testData, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV);
        filterManager.saveImageData(testData,"baboonHighPassFiltered.png");

    }

    @Test
    void testEnlarge(){
        testData = createTestData();
        printData();
        testData.enlarge(9);
        printData();
    }

    @Test
    void testEnlarge2D(){
        testData = createTestData();
        printData();
        testData.enlarge2D(9);
        printData();
    }

    @Test
    void testDownsize2D(){
        testData = createTestData();
        printData();
        testData.enlarge2D(9);
        printData();
        testData.downsize(9);
        printData();
    }

    @Test
    void testDownsize(){
        testData = createTestData();
        printData();
        testData.enlarge(9);
        printData();
        testData.downsize(9);
        printData();
    }
    @Test
    void testHaarFilter(){
        FilterManager manager = new FilterManager();
        testData = new ImageData(2,3);
        testData.setRedDataPixel(0,0,1);
        testData.setRedDataPixel(0,1,2);
        testData.setRedDataPixel(0,2,3);
        testData.setRedDataPixel(1,0,4);
        testData.setRedDataPixel(1,1,5);
        testData.setRedDataPixel(1,2,6);

        printData();
        testData = manager.downSampleImageDataVertical(manager.applyFilter1DVerticalHaar(manager.downSampleImageDataHorizontal
                (manager.applyFilter1DHorizontalHaar(testData, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV)),FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV));
        printData();
    }

    private void printData() {
        System.out.println();
        System.out.println("Print testData:");
        System.out.println();
        for (int i = 0; i < testData.getHeight(); i++ ) {
            for (int j = 0; j < testData.getWidth(); j++) {
                System.out.print(testData.getRedDataPixel(i,j)+" ");
            }
            System.out.println();
        }

    }
}
