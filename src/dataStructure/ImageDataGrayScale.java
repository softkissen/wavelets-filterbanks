package dataStructure;

public class ImageDataGrayScale {
	private int[][] data;

	public ImageDataGrayScale(int height, int width) {
		this.data   = new int[height][width];
	}
	
	public int getDataPixel(int i, int j){
		return data[i][j];
	}

	public int[][] getData() {
		return data;
	}

	public void setData(int[][] redData) {
		this.data = redData;
	}
	
	public void setDataPixel(int i, int j, int value){
		this.data[i][j] = value;
	}

	public void enlarge(int filterLength){
		int[][] dataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];

		for(int i = 0; i < getHeight(); i++){
			for(int j = 0; j < getWidth(); j++){
				dataEnlarged[i+filterLength/2][j+filterLength/2] = data[i][j];
			}
		}

		//Expand columns
		for(int i = filterLength/2; i < filterLength/2 + this.getHeight(); i++) {
			for (int j = filterLength/2 -1; j >= 0; j--) {
				dataEnlarged[i][j] = dataEnlarged[i][j+1];

			}

		}
		for(int i = filterLength/2; i < filterLength/2 + this.getHeight(); i++) {
			for (int j = filterLength/2+this.getWidth(); j < dataEnlarged[0].length; j++) {
				dataEnlarged[i][j] = dataEnlarged[i][j-1];
			}

		}



		//Expand lines
		for(int i = filterLength/2 -1; i >= 0; i--) {
			for (int j = filterLength/2; j <filterLength/2 + this.getWidth(); j++) {
				dataEnlarged[i][j] = dataEnlarged[i+1][j];
			}

		}

		for(int i = filterLength/2 + this.getHeight(); i < dataEnlarged.length; i++) {
			for (int j = filterLength/2; j < filterLength/2 + this.getWidth(); j++) {
				dataEnlarged[i][j] = dataEnlarged[i-1][j];

			}

		}


//		//Expand corners

		int leftUpperCorner = this.getDataPixel(0,0);

		for(int i = 0; i < filterLength/2; i++) {
			for (int j = 0; j < filterLength/2; j++) {
					dataEnlarged[i][j] = leftUpperCorner;
			}
		}

		int rightUpperCorner = this.getDataPixel(0,this.getWidth()-1);


		for(int i = 0; i < filterLength/2; i++) {
			for (int j = filterLength/2+this.getWidth(); j < dataEnlarged[0].length; j++) {
					dataEnlarged[i][j] = rightUpperCorner;
			}
		}
		int leftLowerCorner = this.getDataPixel(this.getHeight()-1,0);

		for(int i = filterLength/2 + this.getHeight(); i < dataEnlarged.length; i++) {
			for (int j = 0; j < filterLength/2; j++) {
					dataEnlarged[i][j] = leftLowerCorner;
			}
		}
		int rightLowerCorner = this.getDataPixel(this.getHeight()-1,this.getWidth()-1);

		for(int i = filterLength/2 + this.getHeight(); i < dataEnlarged.length; i++) {
			for (int j = filterLength/2 + this.getWidth(); j < dataEnlarged[0].length; j++) {
					dataEnlarged[i][j] = rightLowerCorner;
			}
		}
		data = dataEnlarged;
	}

	public void downsize(int filterLength){
		int[][] dataDecreased = new int[getHeight()-(filterLength-1)][getWidth()-(filterLength-1)];
		
		for(int i = 0; i < dataDecreased.length; i++){
			for(int j = 0; j < dataDecreased[0].length; j++){
				dataDecreased[i][j] = data[i+(filterLength/2)][j+(filterLength/2)];
			}
		}

		data = dataDecreased;

	}
	
	public int getHeight(){
		return data.length;
	}
	
	public int getWidth(){
		return data[0].length;
	}

}
