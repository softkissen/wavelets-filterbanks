package dataStructure;

public class ImageData {
	private int[][] redData;
	private int[][] greenData;
	private int[][] blueData;
	
	public ImageData(int height, int width) {
		this.redData   = new int[height][width];
		this.greenData = new int[height][width];
		this.blueData  = new int[height][width];
	}
	
	public int getRedDataPixel(int i, int j){
		return redData[i][j];
	}

	public int[][] getRedData() {
		return redData;
	}

	public void setRedData(int[][] redData) {
		this.redData = redData;
	}
	
	public void setRedDataPixel(int i, int j, int value){
		this.redData[i][j] = value;
	}
	
	public int getGreenDataPixel(int i, int j){
		return greenData[i][j];
	}

	public int[][] getGreenData() {
		return greenData;
	}

	public void setGreenData(int[][] greenData) {
		this.greenData = greenData;
	}
	
	public void setGreenDataPixel(int width, int height, int value){
		this.greenData[width][height] = value;
	}
	
	public int getBlueDataPixel(int i, int j){
		return blueData[i][j];
	}

	public int[][] getBlueData() {
		return blueData;
	}

	public void setBlueData(int[][] blueData) {
		this.blueData = blueData;
	}
	
	public void setBlueDataPixel(int width, int height, int value){
		this.blueData[width][height] = value;
	}

	public void enlarge(int filterLength){
		int[][] redDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];
		int[][] greenDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];
		int[][] blueDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];


		for(int i = 0; i < getHeight(); i++){
			for(int j = 0; j < getWidth(); j++){
				redDataEnlarged[i+filterLength/2][j+filterLength/2] = redData[i][j];
				greenDataEnlarged[i+filterLength/2][j+filterLength/2] = greenData[i][j];
				blueDataEnlarged[i+filterLength/2][j+filterLength/2] = blueData[i][j];
			}
		}



		//Expand columns
		for(int i = filterLength/2; i < filterLength/2 + this.getHeight(); i++) {
			for (int j = filterLength/2 -1; j >= 0; j--) {
				redDataEnlarged[i][j] = redDataEnlarged[i][j+1];
				greenDataEnlarged[i][j] = greenDataEnlarged[i][j+1];
				blueDataEnlarged[i][j] = blueDataEnlarged[i][j+1];
			}

		}
		for(int i = filterLength/2; i < filterLength/2 + this.getHeight(); i++) {
			for (int j = filterLength/2+this.getWidth(); j < redDataEnlarged[0].length; j++) {
				redDataEnlarged[i][j] = redDataEnlarged[i][j-1];
				greenDataEnlarged[i][j] = greenDataEnlarged[i][j-1];
				blueDataEnlarged[i][j] = blueDataEnlarged[i][j-1];
			}

		}



		//Expand lines
		for(int i = filterLength/2 -1; i >= 0; i--) {
			for (int j = filterLength/2; j <filterLength/2 + this.getWidth(); j++) {
				redDataEnlarged[i][j] = redDataEnlarged[i+1][j];
				greenDataEnlarged[i][j] = greenDataEnlarged[i+1][j];
				blueDataEnlarged[i][j] = blueDataEnlarged[i+1][j];
			}

		}

		for(int i = filterLength/2 + this.getHeight(); i < redDataEnlarged.length; i++) {
			for (int j = filterLength/2; j < filterLength/2 + this.getWidth(); j++) {
				redDataEnlarged[i][j] = redDataEnlarged[i-1][j];
				greenDataEnlarged[i][j] = greenDataEnlarged[i-1][j];
				blueDataEnlarged[i][j] = blueDataEnlarged[i-1][j];
			}

		}


		//Expand corners

		int leftUpperCornerRed = this.getRedDataPixel(0,0);
		int leftUpperCornerGreen =this.getGreenDataPixel(0,0);
		int leftUpperCornerBlue = this.getBlueDataPixel(0,0);

		for(int i = 0; i < filterLength/2; i++) {
			for (int j = 0; j < filterLength/2; j++) {
					redDataEnlarged[i][j] = leftUpperCornerRed;
					greenDataEnlarged[i][j] = leftUpperCornerGreen;
					blueDataEnlarged[i][j] = leftUpperCornerBlue;

			}
		}

		int rightUpperCornerRed = this.getRedDataPixel(0,this.getWidth()-1);
		int rightUpperCornerGreen = this.getGreenDataPixel(0,this.getWidth()-1);
		int rightUpperCornerBlue = this.getBlueDataPixel(0,this.getWidth()-1);

		for(int i = 0; i < filterLength/2; i++) {
			for (int j = filterLength/2+this.getWidth(); j < redDataEnlarged[0].length; j++) {
					redDataEnlarged[i][j] = rightUpperCornerRed;
					greenDataEnlarged[i][j] = rightUpperCornerGreen;
					blueDataEnlarged[i][j] = rightUpperCornerBlue;
			}
		}
		int leftLowerCornerRed = this.getRedDataPixel(this.getHeight()-1,0);
		int leftLowerCornerGreen = this.getGreenDataPixel(this.getHeight()-1,0);
		int leftLowerCornerBlue = this.getBlueDataPixel(this.getHeight()-1,0);

		for(int i = filterLength/2 + this.getHeight(); i < redDataEnlarged.length; i++) {
			for (int j = 0; j < filterLength/2; j++) {
					redDataEnlarged[i][j] = leftLowerCornerRed;
					greenDataEnlarged[i][j] = leftLowerCornerGreen;
					blueDataEnlarged[i][j] = leftLowerCornerBlue;
			}
		}
		int rightLowerCornerRed = this.getRedDataPixel(this.getHeight()-1,this.getWidth()-1);
		int rightLowerCornerGreen = this.getGreenDataPixel(this.getHeight()-1,this.getWidth()-1);
		int rightLowerCornerBlue = this.getBlueDataPixel(this.getHeight()-1,this.getWidth()-1);

		for(int i = filterLength/2 + this.getHeight(); i < redDataEnlarged.length; i++) {
			for (int j = filterLength/2 + this.getWidth(); j < redDataEnlarged[0].length; j++) {
					redDataEnlarged[i][j] = rightLowerCornerRed;
					greenDataEnlarged[i][j] = rightLowerCornerGreen;
					blueDataEnlarged[i][j] = rightLowerCornerBlue;
			}
		}




		redData = redDataEnlarged;
		greenData = greenDataEnlarged;
		blueData = blueDataEnlarged;

	}

	public void downsize(int filterLength){
		int[][] redDataDecreased = new int[getHeight()-(filterLength-1)][getWidth()-(filterLength-1)];
		int[][] greenDataDecreased = new int[getHeight()-(filterLength-1)][getWidth()-(filterLength-1)];
		int[][] blueDataDecreased = new int[getHeight()-(filterLength-1)][getWidth()-(filterLength-1)];
		
		for(int i = 0; i < redDataDecreased.length; i++){
			for(int j = 0; j < redDataDecreased[0].length; j++){
				redDataDecreased[i][j] = redData[i+(filterLength/2)][j+(filterLength/2)];
				greenDataDecreased[i][j] = greenData[i+(filterLength/2)][j+(filterLength/2)];
				blueDataDecreased[i][j] = blueData[i+(filterLength/2)][j+(filterLength/2)];
			}
		}

		
		redData = redDataDecreased;
		greenData = greenDataDecreased;
		blueData = blueDataDecreased;
	}
	//TODO works not properly
	public void enlarge2D(int filterLength){
		int[][] redDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];
		int[][] greenDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];
		int[][] blueDataEnlarged = new int[getHeight()+(filterLength-1)][getWidth()+(filterLength-1)];

		for(int i = 0; i < getHeight(); i++){
			for(int j = 0; j < getWidth(); j++){
				redDataEnlarged[i+filterLength/2][j+filterLength/2] = redData[i][j];
				greenDataEnlarged[i+filterLength/2][j+filterLength/2] = greenData[i][j];
				blueDataEnlarged[i+filterLength/2][j+filterLength/2] = blueData[i][j];
			}
		}

		for(int i = 0; i < redDataEnlarged.length; i++){
			for(int j = 0; j < redDataEnlarged[0].length; j++){
				if(i == 0 && j == 0){
					redDataEnlarged[i][j] = redDataEnlarged[i+1][j+1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i+1][j+1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i+1][j+1];
				}
				else if(i == 0 && j == redDataEnlarged[0].length-1){
					redDataEnlarged[i][j] = redDataEnlarged[i+1][j-1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i+1][j-1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i+1][j-1];
				}
				else if(i == redDataEnlarged.length-1 && j == 0){
					redDataEnlarged[i][j] = redDataEnlarged[i-1][j+1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i-1][j+1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i-1][j+1];
				}
				else if(i == redDataEnlarged.length-1 && j == redDataEnlarged[0].length-1){
					redDataEnlarged[i][j] = redDataEnlarged[i-1][j-1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i-1][j-1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i-1][j-1];
				}
				else if(i < filterLength/2){
					redDataEnlarged[i][j] = redDataEnlarged[i+1][j];
					greenDataEnlarged[i][j] = greenDataEnlarged[i+1][j];
					blueDataEnlarged[i][j] = blueDataEnlarged[i+1][j];
				}
				else if(j < filterLength/2){
					redDataEnlarged[i][j] = redDataEnlarged[i][j+1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i][j+1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i][j+1];
				}
				else if(i == redDataEnlarged.length-1){
					redDataEnlarged[i][j] = redDataEnlarged[i-1][j];
					greenDataEnlarged[i][j] = greenDataEnlarged[i-1][j];
					blueDataEnlarged[i][j] = blueDataEnlarged[i-1][j];
				}
				else if(j == redDataEnlarged[0].length-1){
					redDataEnlarged[i][j] = redDataEnlarged[i][j-1];
					greenDataEnlarged[i][j] = greenDataEnlarged[i][j-1];
					blueDataEnlarged[i][j] = blueDataEnlarged[i][j-1];
				}
			}
		}

		redData = redDataEnlarged;
		greenData = greenDataEnlarged;
		blueData = blueDataEnlarged;

	}
	
	public int getHeight(){
		return redData.length;
	}
	
	public int getWidth(){
		return redData[0].length;
	}

}
