package filter;

import dataStructure.ImageData;
import dataStructure.ImageDataGrayScale;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FilterManagerGrayScale {

	public FilterManagerGrayScale(){}

	public ImageDataGrayScale applyFilter1DVerticalHaar(ImageDataGrayScale imageDataGrayScale, float[] filter){
		int filterLength = filter.length;
		ImageDataGrayScale processedImageData = new ImageDataGrayScale(imageDataGrayScale.getHeight(),imageDataGrayScale.getWidth());
		imageDataGrayScale.enlarge(filterLength);

		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalHaar
						(imageDataGrayScale.getData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}


		imageDataGrayScale.downsize(filterLength);

		return processedImageData;
	}

	public ImageDataGrayScale applyFilter1DHorizontalHaar(ImageDataGrayScale imageDataGrayScale, float[] filter){
		int filterLength = filter.length;
		ImageDataGrayScale processedImageData = new ImageDataGrayScale(imageDataGrayScale.getHeight(),imageDataGrayScale.getWidth());
		imageDataGrayScale.enlarge(filterLength);

		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalHaar
						(imageDataGrayScale.getData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}
		imageDataGrayScale.downsize(filterLength);

		return processedImageData;
	}

	private int[] getPixelsForConvolution1DHorizontalHaar(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[3];
		pixelsForConvolution[0]= imageData[i][j-1];
		pixelsForConvolution[1]= imageData[i][j];
		pixelsForConvolution[2]= imageData[i][j+1];
		return pixelsForConvolution;
	}
	


	private int[] getPixelsForConvolution1DVerticalHaar(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[3];
		pixelsForConvolution[0]= imageData[i-1][j];
		pixelsForConvolution[1]= imageData[i][j];
		pixelsForConvolution[2]= imageData[i+1][j];
		return pixelsForConvolution;
	}

	
	private int  pixelConvolution1D(int[] pixels, float[] filter){
		double pixel = 0;
		
		for(int i = 0; i < pixels.length; i++){
				pixel += pixels[i] * filter[i];
		}
		int p = (int) pixel;
		if(p < 0){
			p = 0;
		}
//		if(p > 255){
//			p = 255;
//		}
		return p;
	}
	
//	load image and write pixels into matrix
	public ImageDataGrayScale loadImage(String path) throws FileNotFoundException, IOException{
//		read image
		BufferedImage originalImage = ImageIO.read(new FileInputStream(path));
		ImageDataGrayScale imageDataGrayScale = new ImageDataGrayScale(originalImage.getWidth(),originalImage.getHeight());

		for(int i = 0; i < originalImage.getWidth(); i++){
			for(int j = 0; j < originalImage.getHeight(); j++){
				Color c = new Color(originalImage.getRGB(i, j));
				int red = (int)(c.getRed() * 0.299);
				int green = (int)(c.getGreen() * 0.587);
				int blue = (int)(c.getBlue() *0.114);
				Color newColor = new Color(red+green+blue,
						red+green+blue,red+green+blue);
				imageDataGrayScale.setDataPixel(i,j,newColor.getRGB());

			}
		}
		
		return imageDataGrayScale;
	}

	public ImageDataGrayScale upSampleImageDataHorizontal(ImageDataGrayScale imageDataGrayScale) {
		ImageDataGrayScale upSampledImageDataHorizontal = new ImageDataGrayScale(imageDataGrayScale.getHeight() , imageDataGrayScale.getWidth()*2);
		for(int i = 0; i < upSampledImageDataHorizontal.getHeight(); i++){
			for(int j = 0; j < upSampledImageDataHorizontal.getWidth(); j++){
				if(j % 2 == 0){
					upSampledImageDataHorizontal.setDataPixel(i,j,imageDataGrayScale.getDataPixel(i,j/2));
				}
			}
		}
		return upSampledImageDataHorizontal;
	}


	public ImageDataGrayScale upSampleImageDataVertical(ImageDataGrayScale imageDataGrayScale) {
		ImageDataGrayScale upSampledImageDataVertical = new ImageDataGrayScale(imageDataGrayScale.getHeight()*2 , imageDataGrayScale.getWidth());
		for(int i = 0; i < upSampledImageDataVertical.getHeight(); i++){
			for(int j = 0; j < upSampledImageDataVertical.getWidth(); j++){
				if(i % 2 == 0){
					upSampledImageDataVertical.setDataPixel(i,j,imageDataGrayScale.getDataPixel(i/2,j));
				}
			}
		}
		return upSampledImageDataVertical;
	}





	public ImageDataGrayScale downSampleImageDataHorizontal(ImageDataGrayScale imageDataGrayScale){
		ImageDataGrayScale downSampledImageDataHorizontal = new ImageDataGrayScale(imageDataGrayScale.getHeight(), (int) Math.ceil(((double)imageDataGrayScale.getWidth())/2));
		for(int i = 0; i < downSampledImageDataHorizontal.getHeight(); i++) {
			for (int j = 0; j < downSampledImageDataHorizontal.getWidth(); j++) {
				downSampledImageDataHorizontal.setDataPixel(i, j, imageDataGrayScale.getDataPixel(i , j*2));
			}
		}
		return downSampledImageDataHorizontal;
	}

	public ImageDataGrayScale downSampleImageDataVertical(ImageDataGrayScale imageDataGrayScale){
		ImageDataGrayScale downSampledImageDataVertical = new ImageDataGrayScale((int) Math.ceil(((double)imageDataGrayScale.getHeight())/2) ,imageDataGrayScale.getWidth());
		for(int i = 0; i < downSampledImageDataVertical.getHeight(); i++) {
			for (int j = 0; j < downSampledImageDataVertical.getWidth(); j++) {
				downSampledImageDataVertical.setDataPixel(i, j, imageDataGrayScale.getDataPixel(i*2,j));
			}
		}
		return  downSampledImageDataVertical;
	}


//	sum image data
	public ImageDataGrayScale sumImageData(ImageDataGrayScale imageData1,ImageDataGrayScale imageData2){
		ImageDataGrayScale result = new ImageDataGrayScale(imageData1.getHeight(),imageData1.getWidth());
		
		for(int i = 0; i < result.getHeight(); i++){
			for(int j = 0; j < result.getWidth(); j++){
				result.setDataPixel(i,j,(imageData1.getDataPixel(i,j)+imageData2.getDataPixel(i,j)));
			}
		}
		
		return result;
	}

//	save image from image data matrix
	public ImageDataGrayScale saveImageData(ImageDataGrayScale imageDataGrayScale, String fileName) throws IOException{
		FileOutputStream writer = new FileOutputStream(fileName);
	    
		ImageIO.write(getImageFromData(imageDataGrayScale),"PNG", writer);
		
		return imageDataGrayScale;
	}

//	convert color channels to full picture
	private BufferedImage getImageFromData(ImageDataGrayScale imageDataGrayScale){
		BufferedImage image = new BufferedImage(imageDataGrayScale.getHeight(),imageDataGrayScale.getWidth(),BufferedImage.TYPE_BYTE_GRAY);

		WritableRaster raster = (WritableRaster) image.getData();

	    for(int i = 0; i < imageDataGrayScale.getHeight(); i++) {
	    	for(int j = 0; j < imageDataGrayScale.getWidth(); j++) {
				image.setRGB(i,j,imageDataGrayScale.getDataPixel(i,j));
	    	}
	    }
	    

	    
	    return image;
	}

}
