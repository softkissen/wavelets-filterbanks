package filter.data;

public final class FilterMatrix {
	//H
	public final static float[][] FBI_LOW_PASS = new float[][]{
		{0.0f,-0.06453888262893843863f,-0.04068941760955843672f},
		{0.4180922732222122009f,0.7884856164056643978f,0.4180922732222122009f},
		{-0.04068941760955843672f,-0.06453888262893843863f,0.0f}
	};
	//H
	public final static float[] FBI_LOW_PASS_1D = new float[]{
		0.0f,-0.06453888262893843863f,-0.04068941760955843672f,
		0.4180922732222122009f,0.7884856164056643978f,0.4180922732222122009f,
		-0.04068941760955843672f,-0.06453888262893843863f,0.0f
	};

	public final static float[][] FBI_LOW_PASS_TILDE = new float[][]{
		{0.0378284555069954614f,-0.02384946501938000189f,-0.1106244044184234089f},
		{0.3774028556126537641f,0.8526986790094034193f,0.3774028556126537641f},
		{-0.1106244044184234089f,-0.02384946501938000189f,0.0378284555069954614f}
	};
	public final static float[][] FBI_LOW_PASS_TILDE_TIME_INV = new float[][]{
			{0.0378284555069954614f,-0.02384946501938000189f,-0.1106244044184234089f},
			{0.3774028556126537641f,0.8526986790094034193f,0.3774028556126537641f},
			{-0.1106244044184234089f,-0.02384946501938000189f,0.0378284555069954614f}
	};

	public final static float[] FBI_LOW_PASS_TILDE_1D = new float[]{
		0.0378284555069954614f,-0.02384946501938000189f,-0.1106244044184234089f,
		0.3774028556126537641f,0.8526986790094034193f,0.3774028556126537641f,
		-0.1106244044184234089f,-0.02384946501938000189f,0.0378284555069954614f
	};
	
	public final static float[] FBI_LOW_PASS_TILDE_TIME_INV_1D = new float[]{
		0.0378284555069954614f,-0.02384946501938000189f,-0.1106244044184234089f,
		0.3774028556126537641f,0.8526986790094034193f,0.3774028556126537641f,
		-0.1106244044184234089f,-0.02384946501938000189f,0.0378284555069954614f
	};
	//G
	public final static float[][] FBI_HIGH_PASS = new float[][]{
		{0.0f,-0.0378284555069954614f,-0.0238494650193800189f},
		{0.1106244044184234089f,0.3774028556126537641f,-0.8526986790094034193f},
		{0.3774028556126537641f,0.1106244044184234089f,-0.0238494650193800189f}
	};
	//G
	public final static float[] FBI_HIGH_PASS_1D = new float[]{
		0.0f,-0.0378284555069954614f,-0.0238494650193800189f,
		0.1106244044184234089f,0.3774028556126537641f,-0.8526986790094034193f,
		0.3774028556126537641f,0.1106244044184234089f,-0.0238494650193800189f
	};
	
	public final static float[][] FBI_HIGH_PASS_TILDE = new float[][]{
		{0.0f,0.0f,-0.06453888262893843863f},
		{0.04068941760955843672f,0.4180922732222122009f,-0.7884856164056643978f},
		{0.4180922732222122009f,0.04068941760955843672f,-0.06453888262893843863f}
	};
	
	public final static float[][] FBI_HIGH_PASS_TILDE_TIME_INV = new float[][]{
		{-0.06453888262893843863f,0.04068941760955843672f,0.4180922732222122009f},
		{-0.7884856164056643978f,0.4180922732222122009f,0.04068941760955843672f},
		{-0.06453888262893843863f,0.0f,0.0f}
	};
	
	public final static float[] FBI_HIGH_PASS_TILDE_1D = new float[]{
		0.0f,0.0f,-0.06453888262893843863f,
		0.04068941760955843672f,0.4180922732222122009f,-0.7884856164056643978f,
		0.4180922732222122009f,0.04068941760955843672f,-0.06453888262893843863f
	};
	
	public final static float[] FBI_HIGH_PASS_TILDE_TIME_INV_1D = new float[]{
		-0.06453888262893843863f,0.04068941760955843672f,0.4180922732222122009f,
		-0.7884856164056643978f,0.4180922732222122009f,0.04068941760955843672f,
		-0.06453888262893843863f,0.0f,0.0f
	};
	
	public final static float[][] HAAR_HIGH_PASS = new float[][]{
		{0,0,0},
		{0,(float) (1/Math.sqrt(2)),(float) -(1/Math.sqrt(2))},
		{0,0,0},
	};
	
	public final static float[][] HAAR_HIGH_PASS_TILDE = new float[][]{
		{0,0,0},
		{(float) -(1/Math.sqrt(2)),(float) (1/Math.sqrt(2)),0},
		{0,0,0},
	};
	
	public final static float[][] HAAR_LOW_PASS = new float[][]{
		{0,0,0},
		{0,(float) (1/Math.sqrt(2)),(float) (1/Math.sqrt(2))},
		{0,0,0},
	};
	
	public final static float[][] HAAR_LOW_PASS_TILDE = new float[][]{
		{0,0,0},
		{(float) (1/Math.sqrt(2)),(float) (1/Math.sqrt(2)),0},
		{0,0,0},
	};

	public final static float[] HAAR_LOW_PASS_1D = new float[]{0,(float) (1/Math.sqrt(2)),(float) (1/Math.sqrt(2))};
	public final static float[] HAAR_LOW_PASS_1D_TIME_INV = new float[]{(float) (1/Math.sqrt(2)),(float) (1/Math.sqrt(2)), 0};

	public final static float[] HAAR_HIGH_PASS_1D = new float[]{0, (float) (1/Math.sqrt(2)),(float) -(1/Math.sqrt(2))};
	public final static float[] HAAR_HIGH_PASS_1D_TIME_INV = new float[]{(float) -(1/Math.sqrt(2)),(float) (1/Math.sqrt(2)), 0};


	public final static float[] HAAR_HIGH_PASS_TILDE_1D = new float[]{(float) -(1/Math.sqrt(2)),(float) (1/Math.sqrt(2)),0};
	public final static float[] HAAR_LOW_PASS_TILDE_1D = new float[]{(float) (1/Math.sqrt(2)),(float) (1/Math.sqrt(2)),0};

	public final static float[] VETTERLI_LOW_PASS_1D = new float[]{(float) 0,(float) 1,(float) 2,(float) 1,(float) 0};
	public final static float[] VETTERLI_HIGH_PASS_1D = new float[]{(float) 1,(float) 2,(float) -6,(float) 2,(float) 1};

	public final static float[][] VETTERLI_LOW_PASS_2D = new float[][]{
			{(float) 0,(float) 0,(float) 0,(float) 0,(float) 0},
			{(float) 0,(float) 0,(float) 1,(float) 0,(float) 0},
			{(float) 0,(float) 1,(float) 4,(float) 1,(float) 0},
			{(float) 0,(float) 0,(float) 1,(float) 0,(float) 0},
			{(float) 0,(float) 0,(float) 0,(float) 0,(float) 0},
	};
	public final static float[][] VETTERLI_HIGH_PASS_2D = new float[][]{
			{(float) 0,(float) 0,(float) 1,(float) 0,(float) 0},
			{(float) 0,(float) 2,(float) 4,(float) 2,(float) 0},
			{(float) 1,(float) 4,(float) -28,(float) 4,(float) 1},
			{(float) 0,(float) 2,(float) 4,(float) 2,(float) 0},
			{(float) 0,(float) 0,(float) 1,(float) 0,(float) 0},
	};
	//TODO make Tilde

}
