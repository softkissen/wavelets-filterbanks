package filter;

import dataStructure.ImageData;
import dataStructure.ImageDataGrayScale;
import filter.data.FilterMatrix;

import java.io.IOException;


public class ApplyFilterHigherDimensions {
    private static String imagePath = "hase.png", outputPath = "output/";
    private static ImageData originalImageData;
    private static ImageDataGrayScale originalImageDataGrayScale;
    private static FilterManager manager = new FilterManager();
    private static FilterManagerGrayScale managerGrayScale = new FilterManagerGrayScale();



    public static void main(String[] args) {

        try {
            originalImageData = manager.loadImage(imagePath);
            originalImageDataGrayScale = managerGrayScale.loadImage(imagePath);

            //doSeparableFBI(3);
            doSeparableFBI();
            //doQuincunx(1);
            //doQuincunx();
            //doHaar();
            //doHaarGrayScale();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void doHaarGrayScale() throws IOException {
        ImageDataGrayScale s_0 = originalImageDataGrayScale;
        s_0 = managerGrayScale.applyFilter1DHorizontalHaar(s_0, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        s_0 = managerGrayScale.downSampleImageDataHorizontal(s_0);
        s_0 = managerGrayScale.applyFilter1DVerticalHaar(s_0, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        s_0 = managerGrayScale.downSampleImageDataVertical(s_0);
        managerGrayScale.saveImageData(s_0,outputPath+"Haar/Separable/OneRun/GrayScale/s0.png");

        ImageDataGrayScale w_0H = managerGrayScale.applyFilter1DHorizontalHaar(originalImageDataGrayScale, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        w_0H = managerGrayScale.downSampleImageDataHorizontal(w_0H);
        w_0H = managerGrayScale.applyFilter1DVerticalHaar(w_0H, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0H = managerGrayScale.downSampleImageDataVertical(w_0H);
        managerGrayScale.saveImageData(w_0H,outputPath+"Haar/Separable/OneRun/GrayScale/w0H.png");

        ImageDataGrayScale w_0V = managerGrayScale.applyFilter1DHorizontalHaar(originalImageDataGrayScale, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0V = managerGrayScale.downSampleImageDataHorizontal(w_0V);
        w_0V = managerGrayScale.applyFilter1DVerticalHaar(w_0V, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        w_0V = managerGrayScale.downSampleImageDataVertical(w_0V);
        managerGrayScale.saveImageData(w_0V,outputPath+"Haar/Separable/OneRun/GrayScale/w0V.png");

        ImageDataGrayScale w_0D = managerGrayScale.applyFilter1DHorizontalHaar(originalImageDataGrayScale, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0D = managerGrayScale.downSampleImageDataHorizontal(w_0D);
        w_0D = managerGrayScale.applyFilter1DVerticalHaar(w_0D, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0D = managerGrayScale.downSampleImageDataVertical(w_0D);
        managerGrayScale.saveImageData(w_0D,outputPath+"Haar/Separable/OneRun/GrayScale/w0D.png");



        //wayback
        managerGrayScale.saveImageData(managerGrayScale.sumImageData
                        (managerGrayScale.applyFilter1DHorizontalHaar
                                        (managerGrayScale.upSampleImageDataHorizontal
                                                (managerGrayScale.sumImageData(
                                                        managerGrayScale.applyFilter1DVerticalHaar(managerGrayScale.upSampleImageDataVertical(s_0),FilterMatrix.HAAR_LOW_PASS_1D),
                                                        managerGrayScale.applyFilter1DVerticalHaar(managerGrayScale.upSampleImageDataVertical(w_0H),FilterMatrix.HAAR_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.HAAR_LOW_PASS_1D
                                        ),
                                (managerGrayScale.applyFilter1DHorizontalHaar
                                        (managerGrayScale.upSampleImageDataHorizontal
                                                (managerGrayScale.sumImageData(
                                                        managerGrayScale.applyFilter1DVerticalHaar(managerGrayScale.upSampleImageDataVertical(w_0V),FilterMatrix.HAAR_LOW_PASS_1D),
                                                        managerGrayScale.applyFilter1DVerticalHaar(managerGrayScale.upSampleImageDataVertical(w_0D),FilterMatrix.HAAR_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.HAAR_HIGH_PASS_1D
                                        )
                                )
                        ),
                outputPath+"Haar/Separable/OneRun/GrayScale/Result.png"
        );
    }


    private static void doHaar() throws IOException {
        ImageData s_0 = originalImageData;
        s_0 = manager.applyFilter1DHorizontalHaar(s_0, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        s_0 = manager.downSampleImageDataHorizontal(s_0);
        s_0 = manager.applyFilter1DVerticalHaar(s_0, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        s_0 = manager.downSampleImageDataVertical(s_0);
        manager.saveImageData(s_0,outputPath+"Haar/Separable/OneRun/s0.png");

        ImageData w_0H = manager.applyFilter1DHorizontalHaar(originalImageData, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        w_0H = manager.downSampleImageDataHorizontal(w_0H);
        w_0H = manager.applyFilter1DVerticalHaar(w_0H, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0H = manager.downSampleImageDataVertical(w_0H);
        manager.saveImageData(w_0H,outputPath+"Haar/Separable/OneRun/w0H.png");

        ImageData w_0V = manager.applyFilter1DHorizontalHaar(originalImageData, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0V = manager.downSampleImageDataHorizontal(w_0V);
        w_0V = manager.applyFilter1DVerticalHaar(w_0V, FilterMatrix.HAAR_LOW_PASS_1D_TIME_INV);
        w_0V = manager.downSampleImageDataVertical(w_0V);
        manager.saveImageData(w_0V,outputPath+"Haar/Separable/OneRun/w0V.png");

        ImageData w_0D = manager.applyFilter1DHorizontalHaar(originalImageData, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0D = manager.downSampleImageDataHorizontal(w_0D);
        w_0D = manager.applyFilter1DVerticalHaar(w_0D, FilterMatrix.HAAR_HIGH_PASS_1D_TIME_INV);
        w_0D = manager.downSampleImageDataVertical(w_0D);
        manager.saveImageData(w_0D,outputPath+"Haar/Separable/OneRun/w0D.png");



        //wayback
        manager.saveImageData(manager.sumImageData
                        (manager.applyFilter1DHorizontalHaar
                                        (manager.upSampleImageDataHorizontal
                                                (manager.sumImageData(
                                                        manager.applyFilter1DVerticalHaar(manager.upSampleImageDataVertical(s_0),FilterMatrix.HAAR_LOW_PASS_1D),
                                                        manager.applyFilter1DVerticalHaar(manager.upSampleImageDataVertical(w_0H),FilterMatrix.HAAR_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.HAAR_LOW_PASS_1D
                                        ),
                                (manager.applyFilter1DHorizontalHaar
                                        (manager.upSampleImageDataHorizontal
                                                (manager.sumImageData(
                                                        manager.applyFilter1DVerticalHaar(manager.upSampleImageDataVertical(w_0V),FilterMatrix.HAAR_LOW_PASS_1D),
                                                        manager.applyFilter1DVerticalHaar(manager.upSampleImageDataVertical(w_0D),FilterMatrix.HAAR_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.HAAR_HIGH_PASS_1D
                                        )
                                )
                        ),
                outputPath+"Haar/Separable/OneRun/Result.png"
        );
    }

    private static void doSeparableFBI(int steps) throws IOException {
        ImageData[][] details = new ImageData[steps][3];
        ImageData s_i = originalImageData;
        ImageData w_iH;
        ImageData w_iV;
        ImageData w_iD;

        for(int i = steps-1; i >= 0; i--){

            w_iH = manager.applyFilter1DHorizontalFBI(s_i, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
            w_iH = manager.downSampleImageDataHorizontal(w_iH);
            w_iH = manager.applyFilter1DVerticalFBI(w_iH, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
            w_iH = manager.downSampleImageDataVertical(w_iH);
            details[i][0] = w_iH;
            manager.saveImageData(w_iH,outputPath+"FBI/Separable/Rekursiv/w"+i+"H.png");


            w_iV = manager.applyFilter1DHorizontalFBI(s_i, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
            w_iV = manager.downSampleImageDataHorizontal(w_iV);
            w_iV = manager.applyFilter1DVerticalFBI(w_iV, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
            w_iV = manager.downSampleImageDataVertical(w_iV);
            details[i][1] = w_iV;
            manager.saveImageData(w_iV,outputPath+"FBI/Separable/Rekursiv/w"+i+"V.png");


            w_iD = manager.applyFilter1DHorizontalFBI(s_i, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
            w_iD = manager.downSampleImageDataHorizontal(w_iD);
            w_iD = manager.applyFilter1DVerticalFBI(w_iD, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
            w_iD = manager.downSampleImageDataVertical(w_iD);
            details[i][2] = w_iD;
            manager.saveImageData(w_iD,outputPath+"FBI/Separable/Rekursiv/w"+i+"D.png");


            s_i = manager.applyFilter1DHorizontalFBI(s_i, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
            s_i = manager.downSampleImageDataHorizontal(s_i);
            s_i = manager.applyFilter1DVerticalFBI(s_i, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
            s_i = manager.downSampleImageDataVertical(s_i);
            manager.saveImageData(s_i,outputPath+"FBI/Separable/Rekursiv/s"+i+".png");
        }

        int index = steps -1;
        for(int i = 0; i <= steps-1; i++) {
            w_iH = details[i][0];
            w_iV = details[i][1];
            w_iD = details[i][2];

            index--;
            //wayback
            s_i = manager.saveImageData(manager.sumImageData
                            (manager.applyFilter1DHorizontalFBI
                                            (manager.upSampleImageDataHorizontal
                                                    (manager.sumImageData(
                                                            manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(s_i), FilterMatrix.FBI_LOW_PASS_1D),
                                                            manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_iH), FilterMatrix.FBI_HIGH_PASS_1D)
                                                            )
                                                    ), FilterMatrix.FBI_LOW_PASS_1D
                                            ),
                                    (manager.applyFilter1DHorizontalFBI
                                            (manager.upSampleImageDataHorizontal
                                                    (manager.sumImageData(
                                                            manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_iV), FilterMatrix.FBI_LOW_PASS_1D),
                                                            manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_iD), FilterMatrix.FBI_HIGH_PASS_1D)
                                                            )
                                                    ), FilterMatrix.FBI_HIGH_PASS_1D
                                            )
                                    )
                            ),
                    outputPath + "FBI/Separable/Rekursiv/Result"+(i+1)+".png"
            );
        }
    }
    private static void doSeparableFBI() throws IOException {
        ImageData s_0 = originalImageData;

        ImageData w_0H = manager.applyFilter1DHorizontalFBI(s_0, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        w_0H = manager.downSampleImageDataHorizontal(w_0H);
        w_0H = manager.applyFilter1DVerticalFBI(w_0H, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
        w_0H = manager.downSampleImageDataVertical(w_0H);
        manager.saveImageData(w_0H,outputPath+"FBI/Separable/OneRun/w0H.png");

        ImageData w_0V = manager.applyFilter1DHorizontalFBI(s_0, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
        w_0V = manager.downSampleImageDataHorizontal(w_0V);
        w_0V = manager.applyFilter1DVerticalFBI(w_0V, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        w_0V = manager.downSampleImageDataVertical(w_0V);
        manager.saveImageData(w_0V,outputPath+"FBI/Separable/OneRun/w0V.png");

        ImageData w_0D = manager.applyFilter1DHorizontalFBI(s_0, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
        w_0D = manager.downSampleImageDataHorizontal(w_0D);
        w_0D = manager.applyFilter1DVerticalFBI(w_0D, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D);
        w_0D = manager.downSampleImageDataVertical(w_0D);
        manager.saveImageData(w_0D,outputPath+"FBI/Separable/OneRun/w0D.png");

        s_0 = manager.applyFilter1DHorizontalFBI(s_0, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        s_0 = manager.downSampleImageDataHorizontal(s_0);
        s_0 = manager.applyFilter1DVerticalFBI(s_0, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D);
        s_0 = manager.downSampleImageDataVertical(s_0);
        manager.saveImageData(s_0,outputPath+"FBI/Separable/OneRun/s0.png");
        //wayback
        manager.saveImageData(manager.sumImageData
                        (manager.applyFilter1DHorizontalFBI
                                        (manager.upSampleImageDataHorizontal
                                                (manager.sumImageData(
                                                        manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(s_0),FilterMatrix.FBI_LOW_PASS_1D),
                                                        manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_0H),FilterMatrix.FBI_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.FBI_LOW_PASS_1D
                                        ),
                                (manager.applyFilter1DHorizontalFBI
                                        (manager.upSampleImageDataHorizontal
                                                (manager.sumImageData(
                                                        manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_0V),FilterMatrix.FBI_LOW_PASS_1D),
                                                        manager.applyFilter1DVerticalFBI(manager.upSampleImageDataVertical(w_0D),FilterMatrix.FBI_HIGH_PASS_1D)
                                                        )
                                                ),FilterMatrix.FBI_HIGH_PASS_1D
                                        )
                                )
                        ),
                outputPath+"FBI/Separable/OneRun/Result.png"
        );
    }

    //TODO
    private static void doQuincunx(int steps) throws IOException {
        ImageData s_i = originalImageData;
        ImageData w_i = originalImageData;
        int h;
        int w ;

        for(int i = steps-1; i >= 0; i--){
            h = s_i.getHeight();
            w = s_i.getWidth();
            w_i = manager.applyFilter(w_i, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV);
            s_i = manager.applyFilter(s_i, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV);
            s_i = manager.downSampleImageDataQuincunx(s_i);
            w_i = manager.downSampleImageDataQuincunx(w_i);
            w_i = manager.upSampleImageDataQuincunx(w_i,h,w);
            w_i = manager.applyFilter(w_i, FilterMatrix.FBI_HIGH_PASS);
        }
        for(int i = 0; i <= steps-1; i++){
        s_i = manager.upSampleImageDataQuincunx(s_i,w_i.getHeight(),w_i.getWidth());
        s_i = manager.applyFilter(s_i, FilterMatrix.FBI_LOW_PASS);
        manager.saveImageData(s_i, outputPath+"NonSeparable/Rekursiv/s"+i+"BeforeSumUp.png");
        manager.saveImageData(w_i, outputPath+"NonSeparable/Rekursiv/w"+i+"BeforeSumUp.png");
        s_i = manager.saveImageData(manager.sumImageData(s_i,w_i),outputPath+"NonSeparable/Rekursiv/Result"+i+".png");
        }
    }
    //TODO
    private static void doQuincunx() throws IOException {
        ImageData  s_i = manager.applyFilter(originalImageData, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV);
        s_i = manager.downSampleImageDataQuincunx(s_i);
        s_i = manager.upSampleImageDataQuincunx( s_i,originalImageData.getHeight(),originalImageData.getWidth());
        s_i = manager.applyFilter(s_i, FilterMatrix.FBI_LOW_PASS);
        manager.saveImageData(s_i, outputPath+"NonSeparable/OneRun/s0.png");

        ImageData w_i = manager.applyFilter(originalImageData, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV);
        w_i = manager.downSampleImageDataQuincunx(w_i);
        w_i = manager.upSampleImageDataQuincunx(w_i,originalImageData.getHeight(),originalImageData.getWidth());
        w_i = manager.applyFilter(w_i, FilterMatrix.FBI_HIGH_PASS);
        manager.saveImageData(w_i, outputPath+"NonSeparable/OneRun/w0BeforeSumUp.png");
        manager.saveImageData(s_i, outputPath+"NonSeparable/OneRun/s0BeforeSumUp.png");
        manager.saveImageData(manager.sumImageData(s_i,w_i),outputPath+"NonSeparable/OneRun/Result.png");
    }




}
