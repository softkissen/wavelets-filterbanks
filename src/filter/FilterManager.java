package filter;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import dataStructure.ImageData;

public class FilterManager {

	public FilterManager(){}
	
//	apply filter to the whole image
	public ImageData applyFilter(ImageData imageData, float[][] filter){
		int filterLength = filter.length;
		ImageData processedImageData = new ImageData(imageData.getHeight(),imageData.getWidth());
		imageData.enlarge(filterLength);
		
		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setRedDataPixel(i,j,pixelConvolution(getPixelsForConvolution
						(imageData.getRedData(),i+(filterLength/2),j+(filterLength/2)),filter));
				processedImageData.setGreenDataPixel(i,j,pixelConvolution(getPixelsForConvolution
						(imageData.getGreenData(),i+(filterLength/2),j+(filterLength/2)),filter));
				processedImageData.setBlueDataPixel(i,j,pixelConvolution(getPixelsForConvolution
						(imageData.getBlueData(),i+(filterLength/2),j+(filterLength/2)),filter));
			}
		}
		
		imageData.downsize(filterLength);
		
		return processedImageData;
	}
	
	public ImageData applyFilter1DVerticalFBI(ImageData imageData, float[] filter){
		int filterLength = filter.length;
		ImageData processedImageData = new ImageData(imageData.getHeight(),imageData.getWidth());

		imageData.enlarge(filterLength);
		
		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setRedDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalFBI
						(imageData.getRedData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setGreenDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalFBI
						(imageData.getGreenData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setBlueDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalFBI
						(imageData.getBlueData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}
		
		imageData.downsize(filterLength);
		return processedImageData;
	}

	public ImageData applyFilter1DVerticalHaar(ImageData imageData, float[] filter){
		int filterLength = filter.length;
		ImageData processedImageData = new ImageData(imageData.getHeight(),imageData.getWidth());
		imageData.enlarge(filterLength);

		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setRedDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalHaar
						(imageData.getRedData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setGreenDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalHaar
						(imageData.getGreenData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setBlueDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DVerticalHaar
						(imageData.getBlueData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}

		imageData.downsize(filterLength);

		return processedImageData;
	}

	public ImageData applyFilter1DHorizontalFBI(ImageData imageData, float[] filter){
		int filterLength = filter.length;
		ImageData processedImageData = new ImageData(imageData.getHeight(),imageData.getWidth());
		imageData.enlarge(filterLength);

		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setRedDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalFBI
						(imageData.getRedData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setGreenDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalFBI
						(imageData.getGreenData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setBlueDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalFBI
						(imageData.getBlueData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}

		imageData.downsize(filterLength);

		return processedImageData;
	}

	public ImageData applyFilter1DHorizontalHaar(ImageData imageData, float[] filter){
		int filterLength = filter.length;
		ImageData processedImageData = new ImageData(imageData.getHeight(),imageData.getWidth());
		imageData.enlarge(filterLength);

		for(int i = 0; i < processedImageData.getHeight(); i++){
			for(int j = 0; j < processedImageData.getWidth(); j++){
				processedImageData.setRedDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalHaar
						(imageData.getRedData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setGreenDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalHaar
						(imageData.getGreenData(),i+filterLength/2,j+filterLength/2),filter));
				processedImageData.setBlueDataPixel(i,j,pixelConvolution1D(getPixelsForConvolution1DHorizontalHaar
						(imageData.getBlueData(),i+filterLength/2,j+filterLength/2),filter));
			}
		}

		imageData.downsize(filterLength);

		return processedImageData;
	}



	
//	get pixels for convolution, amount of pixels = amount of filter coefficients
	private int[][] getPixelsForConvolution(int[][] pixels, int i, int j){
		return new int[][]{
				{pixels[i-1][j-1],pixels[i][j-1],pixels[i+1][j-1]},
				{pixels[i-1][j],pixels[i][j],pixels[i+1][j]},
				{pixels[i-1][j+1],pixels[i][j+1],pixels[i+1][j+1]}
		};
	}

	private int[] getPixelsForConvolution1DHorizontalFBI(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[9];
		pixelsForConvolution[0]= imageData[i][j-4];
		pixelsForConvolution[1]= imageData[i][j-3];
		pixelsForConvolution[2]= imageData[i][j-2];
		pixelsForConvolution[3]= imageData[i][j-1];
		pixelsForConvolution[4]= imageData[i][j];
		pixelsForConvolution[5]= imageData[i][j+1];
		pixelsForConvolution[6]= imageData[i][j+2];
		pixelsForConvolution[7]= imageData[i][j+3];
		pixelsForConvolution[8]= imageData[i][j+4];

		return pixelsForConvolution;
	}
	private int[] getPixelsForConvolution1DHorizontalHaar(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[3];
		pixelsForConvolution[0]= imageData[i][j-1];
		pixelsForConvolution[1]= imageData[i][j];
		pixelsForConvolution[2]= imageData[i][j+1];
		return pixelsForConvolution;
	}
	

	private int[] getPixelsForConvolution1DVerticalFBI(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[9];
		pixelsForConvolution[0]= imageData[i-4][j];
		pixelsForConvolution[1]= imageData[i-3][j];
		pixelsForConvolution[2]= imageData[i-2][j];
		pixelsForConvolution[3]= imageData[i-1][j];
		pixelsForConvolution[4]= imageData[i][j];
		pixelsForConvolution[5]= imageData[i+1][j];
		pixelsForConvolution[6]= imageData[i+2][j];
		pixelsForConvolution[7]= imageData[i+3][j];
		pixelsForConvolution[8]= imageData[i+4][j];

		return pixelsForConvolution;
	}
	private int[] getPixelsForConvolution1DVerticalHaar(int[][] imageData, int i, int j){
		int[] pixelsForConvolution = new int[3];
		pixelsForConvolution[0]= imageData[i-1][j];
		pixelsForConvolution[1]= imageData[i][j];
		pixelsForConvolution[2]= imageData[i+1][j];
		return pixelsForConvolution;
	}
	
//	apply filter to one pixel
	private int pixelConvolution(int[][] pixels, float[][] filter){
		int pixel = 0;
		
		for(int i = 0; i < pixels.length; i++){
			for(int j = 0; j < pixels[0].length; j++){
				pixel += pixels[i][j] * filter[i][j];
			}
		}
		int p = (int) pixel;
		if(p < 0){
			p = 0;
		}
		if(p > 255){
			p = 255;

		}
		return p;
	}
	
	private int  pixelConvolution1D(int[] pixels, float[] filter){
		double pixel = 0;
		
		for(int i = 0; i < pixels.length; i++){
				pixel += pixels[i] * filter[i];
		}
		int p = (int) pixel;
		if(p < 0){
			p = 0;
		}
		if(p > 255){
			p = 255;

		}
		return p;
	}
	
//	load image and write pixels into matrix
	public ImageData loadImage(String path) throws FileNotFoundException, IOException{
//		read image
		BufferedImage originalImage = ImageIO.read(new FileInputStream(path));
		ImageData imageData = new ImageData(originalImage.getWidth(),originalImage.getHeight());
		
		Color rgbValue;
//		int r;
//		int g;
//		int b;
//		int average;
//		for(int i = 0; i < originalImage.getWidth(); i++){
//			for(int j = 0; j < originalImage.getHeight(); j++){
//				rgbValue = new Color(originalImage.getRGB(i,j));
//				r = rgbValue.getRed();
//				g = rgbValue.getGreen();
//				b = rgbValue.getBlue();
//				average = (r+g+b)/3;
//				imageData.setRedDataPixel(i,j,average);
//				imageData.setGreenDataPixel(i,j,average);
//				imageData.setBlueDataPixel(i,j,average);
//			}
//		}

		for(int i = 0; i < originalImage.getWidth(); i++){
			for(int j = 0; j < originalImage.getHeight(); j++){
				rgbValue = new Color(originalImage.getRGB(i,j));
				imageData.setRedDataPixel(i,j,rgbValue.getRed());
				imageData.setGreenDataPixel(i,j,rgbValue.getGreen());
				imageData.setBlueDataPixel(i,j,rgbValue.getBlue());
			}
		}
		
		return imageData;
	}
//	up sample image
	public ImageData upSampleImageData(ImageData imageData){
		ImageData upSampledImageData = new ImageData(imageData.getHeight()*2,imageData.getWidth()*2);

		for(int i = 0; i < upSampledImageData.getHeight(); i++){
			for(int j = 0; j < upSampledImageData.getWidth(); j++){
				if(i % 2 == 0 && j % 2 == 0){
					upSampledImageData.setRedDataPixel(i,j,imageData.getRedDataPixel(i/2,j/2));
					upSampledImageData.setGreenDataPixel(i,j,imageData.getGreenDataPixel(i/2,j/2));
					upSampledImageData.setBlueDataPixel(i,j,imageData.getBlueDataPixel(i/2,j/2));
				}
			}
		}
		
//		fill empty pixel - vertical
		for(int i = 0; i < upSampledImageData.getHeight()-1; i++){
			for(int j = 0; j < upSampledImageData.getWidth(); j++){
				if(i % 2 != 0)
					i++;
				
				if(j == upSampledImageData.getWidth()-1){
					upSampledImageData.setRedDataPixel(i,j,upSampledImageData.getRedDataPixel(i,j-1));
					upSampledImageData.setGreenDataPixel(i,j,upSampledImageData.getGreenDataPixel(i,j-1));
					upSampledImageData.setBlueDataPixel(i,j,upSampledImageData.getBlueDataPixel(i,j-1));
				}
				else if(j % 2 != 0){
					upSampledImageData.setRedDataPixel(i,j,getUpsampledPixelVertical(i,j,upSampledImageData.getRedData()));
					upSampledImageData.setGreenDataPixel(i,j,getUpsampledPixelVertical(i,j,upSampledImageData.getGreenData()));
					upSampledImageData.setBlueDataPixel(i,j,getUpsampledPixelVertical(i,j,upSampledImageData.getBlueData()));
				}
			}
		}
		
//		fill empty pixel - horizontal
		for(int j = 0; j < upSampledImageData.getWidth(); j++){
			for(int i = 0; i < upSampledImageData.getHeight(); i++){
				if(i % 2 == 0)
					i++;
				
				if(i == upSampledImageData.getHeight()-1){
					upSampledImageData.setRedDataPixel(i,j,upSampledImageData.getRedDataPixel(i-1,j));
					upSampledImageData.setGreenDataPixel(i,j,upSampledImageData.getGreenDataPixel(i-1,j));
					upSampledImageData.setBlueDataPixel(i,j,upSampledImageData.getBlueDataPixel(i-1,j));
				}
				else{
					upSampledImageData.setRedDataPixel(i,j,getUpsampledPixelHorizontal(i,j,upSampledImageData.getRedData()));
					upSampledImageData.setGreenDataPixel(i,j,getUpsampledPixelHorizontal(i,j,upSampledImageData.getGreenData()));
					upSampledImageData.setBlueDataPixel(i,j,getUpsampledPixelHorizontal(i,j,upSampledImageData.getBlueData()));
				}
			}
		}
		
		return upSampledImageData;
	}



	public ImageData upSampleImageDataHorizontal(ImageData imageData) {
		ImageData upSampledImageDataHorizontal = new ImageData(imageData.getHeight() , imageData.getWidth()*2);
		for(int i = 0; i < upSampledImageDataHorizontal.getHeight(); i++){
			for(int j = 0; j < upSampledImageDataHorizontal.getWidth(); j++){
				if(j % 2 == 0){
					upSampledImageDataHorizontal.setRedDataPixel(i,j,imageData.getRedDataPixel(i,j/2));
					upSampledImageDataHorizontal.setGreenDataPixel(i,j,imageData.getGreenDataPixel(i,j/2));
					upSampledImageDataHorizontal.setBlueDataPixel(i,j,imageData.getBlueDataPixel(i,j/2));
				}
			}
		}
		return upSampledImageDataHorizontal;
	}


 	public ImageData upSampleImageDataVertical(ImageData imageData) {
		ImageData upSampledImageDataVertical = new ImageData(imageData.getHeight()*2 , imageData.getWidth());
		for(int i = 0; i < upSampledImageDataVertical.getHeight(); i++){
			for(int j = 0; j < upSampledImageDataVertical.getWidth(); j++){
				if(i % 2 == 0){
					upSampledImageDataVertical.setRedDataPixel(i,j,imageData.getRedDataPixel(i/2,j));
					upSampledImageDataVertical.setGreenDataPixel(i,j,imageData.getGreenDataPixel(i/2,j));
					upSampledImageDataVertical.setBlueDataPixel(i,j,imageData.getBlueDataPixel(i/2,j));
				}
			}
		}
		return upSampledImageDataVertical;
	}


 	//TODO
	public ImageData upSampleImageDataQuincunx(ImageData imageData, int originalHeigth, int originalWidth) {

		ImageData resultImage = new ImageData(originalHeigth, originalWidth);
		int counter = 0;
		for(int i = 0; i < resultImage.getHeight(); i++){
			for(int j = 0; j < resultImage.getWidth(); j++){
				if((i+j)%2 != 0){
					resultImage.setRedDataPixel(i, j, 0);
					resultImage.setGreenDataPixel(i, j, 0);
					resultImage.setBlueDataPixel(i, j, 0);
				}
				else{
					resultImage.setRedDataPixel(i, j, imageData.getRedDataPixel(0, counter));
					resultImage.setGreenDataPixel(i, j, imageData.getGreenDataPixel(0, counter));
					resultImage.setBlueDataPixel(i, j, imageData.getBlueDataPixel(0, counter));
					counter++;
				}
			}
		}

		return resultImage;

	}

	private int getUpsampledPixelVertical(int i, int j, int[][] pixels){
		return (pixels[i][j-1] + pixels[i][j+1])/2;
	}
	
	private int getUpsampledPixelHorizontal(int i, int j, int[][] pixels){
		return (pixels[i-1][j] + pixels[i+1][j])/2;
	}
	
//	down sample image
	public ImageData downSampleImageData(ImageData imageData){
		ImageData downSampledImageData = new ImageData(imageData.getHeight()/2,imageData.getWidth()/2);
		for(int i = 0; i < downSampledImageData.getHeight(); i++){
			for(int j = 0; j < downSampledImageData.getWidth(); j++){
				downSampledImageData.setRedDataPixel(i,j,imageData.getRedDataPixel(i*2,j*2));
				downSampledImageData.setGreenDataPixel(i,j,imageData.getGreenDataPixel(i*2,j*2));
				downSampledImageData.setBlueDataPixel(i,j,imageData.getBlueDataPixel(i*2,j*2));
			}
		}
		
		return downSampledImageData;
	}


	public ImageData downSampleImageDataHorizontal(ImageData imageData){
		ImageData downSampledImageDataHorizontal = new ImageData(imageData.getHeight(), (int) Math.ceil(((double)imageData.getWidth())/2));
		for(int i = 0; i < downSampledImageDataHorizontal.getHeight(); i++) {
			for (int j = 0; j < downSampledImageDataHorizontal.getWidth(); j++) {
				downSampledImageDataHorizontal.setRedDataPixel(i, j, imageData.getRedDataPixel(i , j*2));
				downSampledImageDataHorizontal.setGreenDataPixel(i, j, imageData.getGreenDataPixel(i , j*2));
				downSampledImageDataHorizontal.setBlueDataPixel(i, j, imageData.getBlueDataPixel(i , j*2));
			}
		}
		return downSampledImageDataHorizontal;
	}

	public ImageData downSampleImageDataVertical(ImageData imageData){
		ImageData downSampledImageDataVertical = new ImageData((int) Math.ceil(((double)imageData.getHeight())/2) ,imageData.getWidth());
		for(int i = 0; i < downSampledImageDataVertical.getHeight(); i++) {
			for (int j = 0; j < downSampledImageDataVertical.getWidth(); j++) {
				downSampledImageDataVertical.setRedDataPixel(i, j, imageData.getRedDataPixel(i*2,j));
				downSampledImageDataVertical.setGreenDataPixel(i, j, imageData.getGreenDataPixel(i*2, j));
				downSampledImageDataVertical.setBlueDataPixel(i, j, imageData.getBlueDataPixel(i*2, j));
			}
		}
		return  downSampledImageDataVertical;
	}

	//TODO
	public ImageData downSampleImageDataQuincunx(ImageData imageData){

		for(int i = 0; i < imageData.getHeight(); i++) {
			for (int j = 0; j < imageData.getWidth(); j++) {
				if(imageData.getBlueDataPixel(i,j)== -5000 || imageData.getGreenDataPixel(i,j)== -5000 || imageData.getRedDataPixel(i,j)== -5000 ){
					System.out.println("-5000");
				}
				if((i+j)%2 != 0){
					imageData.setRedDataPixel(i, j, -5000);
					imageData.setGreenDataPixel(i, j, -5000);
					imageData.setBlueDataPixel(i, j, -5000);
				}
				else{
					imageData.setRedDataPixel(i, j, imageData.getRedDataPixel(i,j));
					imageData.setGreenDataPixel(i, j, imageData.getGreenDataPixel(i, j));
					imageData.setBlueDataPixel(i, j, imageData.getBlueDataPixel(i, j));

				}

			}
		}
		List<Integer> redDataPixels = new ArrayList<>();
		List<Integer> greenDataPixels = new ArrayList<>();
		List<Integer> blueDataPixels = new ArrayList<>();

		for(int i = 0; i < imageData.getHeight(); i++) {
			for (int j = 0; j < imageData.getWidth(); j++) {
				if(imageData.getRedDataPixel(i,j) != -5000){
					redDataPixels.add(imageData.getRedDataPixel(i,j));
					greenDataPixels.add(imageData.getGreenDataPixel(i,j));
					blueDataPixels.add(imageData.getBlueDataPixel(i,j));
				}
			}
		}


		ImageData downSampledImageDataQuincunx = new ImageData(1,redDataPixels.size());
		for(int i = 0; i < downSampledImageDataQuincunx.getHeight(); i++) {
			for (int j = 0; j < downSampledImageDataQuincunx.getWidth(); j++) {
					downSampledImageDataQuincunx.setRedDataPixel(i,j,redDataPixels.get(j));
					downSampledImageDataQuincunx.setGreenDataPixel(i,j,greenDataPixels.get(j));
					downSampledImageDataQuincunx.setBlueDataPixel(i,j,blueDataPixels.get(j));
			}
		}

		return downSampledImageDataQuincunx;
	}

//	sum image data
	public ImageData sumImageData(ImageData imageData1,ImageData imageData2){
		ImageData result = new ImageData(imageData1.getHeight(),imageData1.getWidth());
		
		for(int i = 0; i < result.getHeight(); i++){
			for(int j = 0; j < result.getWidth(); j++){
				result.setRedDataPixel(i,j,(imageData1.getRedDataPixel(i,j)+imageData2.getRedDataPixel(i,j)));
				result.setGreenDataPixel(i,j,(imageData1.getGreenDataPixel(i,j)+imageData2.getGreenDataPixel(i,j)));
				result.setBlueDataPixel(i,j,(imageData1.getBlueDataPixel(i,j)+imageData2.getBlueDataPixel(i,j)));
			}
		}
		
		return result;
	}

//	save image from image data matrix
	public ImageData saveImageData(ImageData imageData, String fileName) throws IOException{
		FileOutputStream writer = new FileOutputStream(fileName);
	    
		ImageIO.write(getImageFromData(imageData),"PNG", writer);
		
		return imageData;
	}

	public void compareImages(ImageData imageData1, ImageData imageData2, String fileName) throws IOException{
		ImageData comparisionImage = new ImageData(imageData1.getHeight(),imageData1.getWidth());

		for(int i = 0; i < imageData1.getHeight(); i++) {
	    	for(int j = 0; j < imageData1.getWidth(); j++) {
	    		if(imageData1.getRedDataPixel(i,j) == imageData2.getRedDataPixel(i,j) &&
	    		   imageData1.getGreenDataPixel(i,j) == imageData2.getGreenDataPixel(i,j) &&
	    		   imageData1.getBlueDataPixel(i,j) == imageData2.getBlueDataPixel(i,j)){
	    			comparisionImage.setGreenDataPixel(i,j,255);
	    		}
	    	}
		}

		saveImageData(comparisionImage, fileName);
	}
	
//	convert color channels to full picture
	private BufferedImage getImageFromData(ImageData imageData){
		BufferedImage image = new BufferedImage(imageData.getHeight(),imageData.getWidth(),BufferedImage.TYPE_INT_RGB);
		
		WritableRaster raster = (WritableRaster) image.getData();
		
	    for(int i = 0; i < imageData.getHeight(); i++) {
	    	for(int j = 0; j < imageData.getWidth(); j++) {
	    		raster.setPixel(i,j,new int[]{imageData.getRedData()[i][j],imageData.getGreenData()[i][j],imageData.getBlueData()[i][j]});
	    	}
	    }
	    
	    image.setData(raster);
	    
	    return image;
	}

}
