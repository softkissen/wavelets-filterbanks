package filter;

import java.io.FileNotFoundException;
import java.io.IOException;

import dataStructure.ImageData;

import filter.data.FilterMatrix;

public class ApplyFilter {

	private static String imagePath = "hase.png", outputPath = "output/";
	private static ImageData originalImageData;

	public static void main(String[] args) {
		FilterManager manager = new FilterManager();

		try {

			originalImageData = manager.loadImage(imagePath);
			
			manager.saveImageData(manager.upSampleImageData(originalImageData),"upsampled.png");
			manager.saveImageData(manager.downSampleImageData(originalImageData),"downsampled.png");
			
/***************************************************************************************************************
 * HAAR
 */
//			1D
			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.HAAR_LOW_PASS_1D),outputPath+"Haar/1D/"+"LOW_PASS.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.HAAR_LOW_PASS_TILDE_1D),outputPath+"Haar/1D/"+"LOW_PASS_TILDE.png");
			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.HAAR_HIGH_PASS_1D),outputPath+"Haar/1D/"+"HIGH_PASS.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.HAAR_HIGH_PASS_TILDE_1D),outputPath+"Haar/1D/"+"HIGH_PASS_TILDE.png");
	
//			Output
//			without sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(
							manager.applyFilter1DHorizontalFBI(manager.applyFilter1DHorizontalFBI(originalImageData, FilterMatrix.HAAR_HIGH_PASS_TILDE_1D),FilterMatrix.HAAR_HIGH_PASS_1D),outputPath + "Haar/1D/withoutSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(
							manager.applyFilter1DHorizontalFBI(manager.applyFilter1DHorizontalFBI(originalImageData, FilterMatrix.HAAR_LOW_PASS_TILDE_1D),FilterMatrix.HAAR_LOW_PASS_1D),outputPath + "Haar/1D/withoutSampling/"+"Differenz_HIGH.png")),outputPath+"Haar/1D/withoutSampling/"+"HIGH+LOW_WITHOUT_SAMPLING.png");

//			with sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(manager.applyFilter1DVerticalFBI(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter1DVerticalFBI(originalImageData,FilterMatrix.HAAR_LOW_PASS_TILDE_1D)),outputPath+"Haar/1D/withSampling/"+"Analyse_LOW.png")),FilterMatrix.HAAR_LOW_PASS_1D),outputPath+"Haar/1D/withSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(manager.applyFilter1DVerticalFBI(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter1DVerticalFBI(originalImageData,FilterMatrix.HAAR_HIGH_PASS_TILDE_1D)),outputPath+"Haar/1D/withSampling/"+"Analyse_HIGH.png")),FilterMatrix.HAAR_HIGH_PASS_1D),outputPath+"Haar/1D/withSampling/"+"Differenz_HIGH.png")),outputPath+"Haar/1D/withSampling/"+"HIGH+LOW_WITH_SAMPLING.png");
			

//			2D
//			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.HAAR_LOW_PASS_TILDE),outputPath+"Haar/2D/"+"LOW_PASS_TILDE.png");
			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.HAAR_LOW_PASS),outputPath+"Haar/2D/"+"LOW_PASS.png");
//			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.HAAR_HIGH_PASS_TILDE),outputPath+"Haar/2D/"+"HIGH_PASS_TILDE.png");
			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.HAAR_HIGH_PASS),outputPath+"Haar/2D/"+"HIGH_PASS.png");

//			Output
//			without sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(
					manager.applyFilter(manager.applyFilter(originalImageData, FilterMatrix.HAAR_HIGH_PASS_TILDE),FilterMatrix.HAAR_HIGH_PASS),outputPath + "Haar/2D/withoutSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(
					manager.applyFilter(manager.applyFilter(originalImageData, FilterMatrix.HAAR_LOW_PASS_TILDE),FilterMatrix.HAAR_LOW_PASS),outputPath + "Haar/2D/withoutSampling/"+"Differenz_HIGH.png")),outputPath+"Haar/2D/withoutSampling/"+"HIGH+LOW_WITHOUT_SAMPLING.png");
			
//			with sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(manager.applyFilter(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter(originalImageData,FilterMatrix.HAAR_LOW_PASS_TILDE)),outputPath+"Haar/2D/withSampling/"+"Analyse_LOW.png")),FilterMatrix.HAAR_LOW_PASS),outputPath+"Haar/2D/withSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(manager.applyFilter(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter(originalImageData,FilterMatrix.HAAR_HIGH_PASS_TILDE)),outputPath+"Haar/2D/withSampling/"+"Analyse_HIGH.png")),FilterMatrix.HAAR_HIGH_PASS),outputPath+"Haar/2D/withSampling/"+"Differenz_HIGH.png")),outputPath+"Haar/2D/withSampling/"+"HIGH+LOW_WITH_SAMPLING.png");
			
/***************************************************************************************************************
 * FBI
 */
//			1D
			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_LOW_PASS_1D),outputPath+"FBI/1D/"+"LOW_PASS.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_LOW_PASS_TILDE_1D),outputPath+"FBI/1D/"+"LOW_PASS_TILDE.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D),outputPath+"FBI/1D/"+"LOW_PASS_TILDE_TIME_INV.png");
			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_HIGH_PASS_1D),outputPath+"FBI/1D/"+"HIGH_PASS.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_HIGH_PASS_TILDE_1D),outputPath+"FBI/1D/"+"HIGH_PASS_TILDE.png");
//			manager.saveImageData(manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D),outputPath+"FBI/1D/"+"HIGH_PASS_TILDE_TIME_INV.png");
			
//			Output
//			without sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(
							manager.applyFilter1DHorizontalFBI(manager.applyFilter1DHorizontalFBI(originalImageData, FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D),FilterMatrix.FBI_LOW_PASS_1D),outputPath + "FBI/1D/withoutSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(
							manager.applyFilter1DHorizontalFBI(manager.applyFilter1DHorizontalFBI(originalImageData, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D),FilterMatrix.FBI_HIGH_PASS_1D),outputPath + "FBI/1D/withoutSampling/"+"Differenz_HIGH.png")),outputPath+"FBI/1D/withoutSampling/"+"HIGH+LOW_WITHOUT_SAMPLING.png");

//			with sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(manager.applyFilter1DHorizontalFBI(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV_1D)),outputPath+"FBI/1D/withSampling/"+"Analyse_HIGH.png")),FilterMatrix.FBI_HIGH_PASS_1D),outputPath+"FBI/1D/withSampling/"+"Differenz_HIGH.png"),
//					high pass
					manager.saveImageData(manager.applyFilter1DHorizontalFBI(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter1DHorizontalFBI(originalImageData,FilterMatrix.FBI_LOW_PASS_TILDE_TIME_INV_1D)),outputPath+"FBI/1D/withSampling/"+"Analyse_LOW.png")),FilterMatrix.FBI_LOW_PASS_1D),outputPath+"FBI/1D/withSampling/"+"Differenz_LOW.png")),outputPath+"FBI/1D/withSampling/"+"HIGH+LOW_WITH_SAMPLING.png");
			
//			2D
//			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.FBI_LOW_PASS_TILDE),outputPath+"FBI/2D/"+"FBI_LOW_PASS_TILDE.png");
			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.FBI_LOW_PASS),outputPath+"FBI/2D/"+"FBI_LOW_PASS.png");
//			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.FBI_HIGH_PASS_TILDE),outputPath+"FBI/2D/"+"FBI_HIGH_PASS_TILDE.png");
			manager.saveImageData(manager.applyFilter(originalImageData,FilterMatrix.FBI_HIGH_PASS),outputPath+"FBI/2D/"+"FBI_HIGH_PASS.png");

//			Output
//			without sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(
					manager.applyFilter(manager.applyFilter(originalImageData, FilterMatrix.FBI_LOW_PASS_TILDE),FilterMatrix.FBI_LOW_PASS),outputPath + "FBI/2D/withoutSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(
					manager.applyFilter(manager.applyFilter(originalImageData, FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV),FilterMatrix.FBI_HIGH_PASS),outputPath + "FBI/2D/withoutSampling/"+"Differenz_HIGH.png")),outputPath+"FBI/2D/withoutSampling/"+"HIGH+LOW_WITHOUT_SAMPLING.png");
			
//			with sampling
			manager.saveImageData(manager.sumImageData(
//					low pass
					manager.saveImageData(manager.applyFilter(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter(originalImageData,FilterMatrix.FBI_LOW_PASS_TILDE)),outputPath+"FBI/2D/withSampling/"+"Analyse_LOW.png")),FilterMatrix.FBI_LOW_PASS),outputPath+"FBI/2D/withSampling/"+"Differenz_LOW.png"),
//					high pass
					manager.saveImageData(manager.applyFilter(manager.upSampleImageData(
							manager.saveImageData(manager.downSampleImageData(
									manager.applyFilter(originalImageData,FilterMatrix.FBI_HIGH_PASS_TILDE_TIME_INV)),outputPath+"FBI/2D/withSampling/"+"Analyse_HIGH.png")),FilterMatrix.FBI_HIGH_PASS),outputPath+"FBI/2D/withSampling/"+"Differenz_HIGH.png")),outputPath+"FBI/2D/withSampling/"+"HIGH+LOW_WITH_SAMPLING.png");
			
		} catch (FileNotFoundException e) {
			System.out.println("Image " + imagePath + " not found!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
